/*******************************************************************************
 * Copyright (c) 2011 Laurent CARON.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Laurent CARON (laurent.caron@gmail.com) - initial API and implementation
 *******************************************************************************/

package org.mihalis.opal.multiChoice;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Shell;

/**
 * Classes which extend this abstract class provide methods that deal with the
 * events that are generated when selection occurs in a MultiChoice control.
 *
 * @param <T> the generic type
 * @see MultiChoiceSelectionEvent
 */
public abstract class MultiChoiceSelectionListener<T> implements SelectionListener {
	
	/** The parent. */
	private final MultiChoice<T> parent;

	/**
	 * Instantiates a new multi choice selection listener.
	 *
	 * @param parent the parent
	 */
	public MultiChoiceSelectionListener(final MultiChoice<T> parent) {
		this.parent = parent;
	}

	/**
	 * Widget selected.
	 *
	 * @param e the e
	 * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
	 */
	@Override
	public final void widgetSelected(final SelectionEvent e) {
		final Button button = (Button) e.widget;
		handle(this.parent, this.parent.getLastModified(), button.getSelection(), this.parent.getPopup());
	}

	/**
	 * Widget default selected.
	 *
	 * @param inutile the inutile
	 * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent)
	 */
	@Override
	public final void widgetDefaultSelected(final SelectionEvent inutile) {
	}

	/**
	 * This method contains the code that is called when the selection has
	 * changed.
	 *
	 * @param parent MultiChoice responsible of the event
	 * @param receiver Object modified
	 * @param selected If <code>true</code>, the check box has been checked
	 * @param popup the popup window that contains all checkboxes
	 */
	public abstract void handle(MultiChoice<T> parent, T receiver, boolean selected, Shell popup);
}
