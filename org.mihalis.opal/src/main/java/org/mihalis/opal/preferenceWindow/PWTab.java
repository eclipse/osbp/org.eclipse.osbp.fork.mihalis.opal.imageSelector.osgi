/*******************************************************************************
 * Copyright (c) 2011 Laurent CARON
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Laurent CARON (laurent.caron at gmail dot com) - Initial implementation and API
 *******************************************************************************/
package org.mihalis.opal.preferenceWindow;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.mihalis.opal.preferenceWindow.widgets.PWWidget;

/**
 * Instance of this class are tabs.
 */
public class PWTab extends PWContainer {
	
	/** The image. */
	private final Image image;
	
	/** The text. */
	private final String text;
	
	/** The children. */
	private final List<PWRowGroup> children;

	/**
	 * Constructor.
	 *
	 * @param image image associated to the tab
	 * @param text text associated to the tab
	 */
	PWTab(final Image image, final String text) {
		this.image = image;
		this.text = text;
		this.children = new ArrayList<PWRowGroup>();
	}

	/**
	 * Adds the.
	 *
	 * @param element the element
	 * @return the PW container
	 * @see org.mihalis.opal.preferenceWindow.PWContainer#add(org.mihalis.opal.preferenceWindow.PWContainer)
	 */
	@Override
	public PWContainer add(final PWContainer element) {
		if (!(element instanceof PWGroup) && !(element instanceof PWRow)) {
			throw new UnsupportedOperationException("Can only add a PWGroup or a PWRow.");
		}
		((PWRowGroup) element).setParent(this);
		this.children.add((PWRowGroup) element);
		return this;
	}

	/**
	 * Adds the.
	 *
	 * @param widget the widget
	 * @return the PW container
	 * @see org.mihalis.opal.preferenceWindow.PWContainer#add(org.mihalis.opal.preferenceWindow.widgets.PWWidget)
	 */
	@Override
	public PWContainer add(final PWWidget widget) {
		final PWRow row = new PWRow();
		row.setParent(this);
		row.add(widget);
		this.children.add(row);
		return this;
	}

	/**
	 * Builds the.
	 *
	 * @param parent the parent
	 * @see org.mihalis.opal.preferenceWindow.PWContainer#build(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void build(final Composite parent) {
		final int numberOfColumns = computeNumberOfColums();
		parent.setLayout(new GridLayout(numberOfColumns, false));

		for (final PWRowGroup rowGroup : this.children) {
			rowGroup.setParentNumberOfColumns(numberOfColumns);
			rowGroup.build(parent);
		}

		PreferenceWindow.getInstance().fireEnablers();

	}

	/**
	 * Compute number of colums.
	 *
	 * @return the total number of columns in this tab
	 */
	private int computeNumberOfColums() {
		int numberOfColumns = 1;
		for (final PWRowGroup rowGroup : this.children) {
			if (rowGroup instanceof PWRow) {
				numberOfColumns = Math.max(numberOfColumns, rowGroup.getNumberOfColums());
			}
		}
		return numberOfColumns;
	}

	/**
	 * Gets the image.
	 *
	 * @return the image associate to this tab
	 */
	public Image getImage() {
		return this.image;
	}

	/**
	 * Gets the text.
	 *
	 * @return the text associated to this tab
	 */
	public String getText() {
		return this.text;
	}

}
