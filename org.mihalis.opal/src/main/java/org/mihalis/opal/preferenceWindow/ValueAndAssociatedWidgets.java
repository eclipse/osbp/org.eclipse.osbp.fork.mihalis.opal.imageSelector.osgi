/*******************************************************************************
 * Copyright (c) 2011 Laurent CARON
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Laurent CARON (laurent.caron at gmail dot com) - Initial implementation and API
 *******************************************************************************/
package org.mihalis.opal.preferenceWindow;

import java.util.ArrayList;
import java.util.List;

import org.mihalis.opal.preferenceWindow.widgets.PWWidget;

/**
 * This POJO contains a value a list of widgets that depends on a given
 * property.
 */
class ValueAndAssociatedWidgets {
	
	/** The value. */
	private Object value;
	
	/** The widgets. */
	private final List<PWWidget> widgets;
	
	/** The row groups. */
	private final List<PWRowGroup> rowGroups;

	/**
	 * Constructor.
	 *
	 * @param value associated value
	 */
	ValueAndAssociatedWidgets(final Object value) {
		this.value = value;
		this.widgets = new ArrayList<PWWidget>();
		this.rowGroups = new ArrayList<PWRowGroup>();
	}

	/**
	 * Adds the widget.
	 *
	 * @param widget dependant widget
	 */
	void addWidget(final PWWidget widget) {
		this.widgets.add(widget);
	}

	/**
	 * Adds the row group.
	 *
	 * @param rowGroup dependant row or group
	 */
	void addRowGroup(final PWRowGroup rowGroup) {
		this.rowGroups.add(rowGroup);
	}

	/**
	 * Gets the value.
	 *
	 * @return the value stored in the instance
	 */
	Object getValue() {
		return this.value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value new value stored in this instance
	 */
	void setValue(final Object value) {
		this.value = value;
		fireValueChanged();
	}

	/**
	 * Fire events when the value has changed.
	 */
	void fireValueChanged() {
		for (final PWRowGroup rowGroup : this.rowGroups) {
			rowGroup.enableOrDisable();
		}

		for (final PWWidget widget : this.widgets) {
			widget.enableOrDisable();
		}
	}
}
