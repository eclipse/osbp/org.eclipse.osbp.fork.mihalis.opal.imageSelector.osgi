/*******************************************************************************
 * Copyright (c) 2011 Laurent CARON
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Laurent CARON (laurent.caron at gmail dot com) - Initial implementation and API
 *******************************************************************************/
package org.mihalis.opal.preferenceWindow.widgets;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.mihalis.opal.preferenceWindow.enabler.Enabler;

/**
 * This class is the root class for all widgets that take part of a preference
 * window.
 */
public abstract class PWWidget {
	
	/** The property key. */
	private final String propertyKey;
	
	/** The label. */
	private final String label;
	
	/** The enabler. */
	protected Enabler enabler;
	
	/** The controls. */
	private final List<Control> controls;

	/** The alignment. */
	private int alignment = GridData.BEGINNING;
	
	/** The indent. */
	private int indent = 0;
	
	/** The width. */
	private int width = 100;
	
	/** The height. */
	private int height = -1;
	
	/** The number of columns. */
	protected int numberOfColumns = 1;
	
	/** The grab excess space. */
	private boolean grabExcessSpace = false;

	/** The single widget. */
	private boolean singleWidget = false;

	/**
	 * Constructor.
	 *
	 * @param label label associated to the widget
	 * @param propertyKey property key binded to the widget
	 * @param numberOfColumns number of columns taken by the widget
	 * @param singleWidget if true, the widget is supposed to be "alone" (used
	 *            for placement)
	 */
	protected PWWidget(final String label, final String propertyKey, final int numberOfColumns, final boolean singleWidget) {
		this.label = label;
		this.propertyKey = propertyKey;
		this.numberOfColumns = numberOfColumns;
		this.singleWidget = singleWidget;
		this.controls = new ArrayList<Control>();
	}

	/**
	 * Build the widget.
	 *
	 * @param parent parent composite
	 * @return the created control
	 */
	protected abstract Control build(Composite parent);

	/**
	 * Build the label associated to the widget.
	 *
	 * @param parent parent composite
	 * @param verticalAlignment vertical alignment
	 */
	protected void buildLabel(final Composite parent, final int verticalAlignment) {
		if (getLabel() != null) {
			final Label label = new Label(parent, SWT.NONE);
			label.setText(getLabel());
			final GridData labelGridData = new GridData(GridData.END, verticalAlignment, false, false);
			labelGridData.horizontalIndent = getIndent();
			label.setLayoutData(labelGridData);
			addControl(label);
		}
	}

	/**
	 * Check if the property can be binded to the widget.
	 *
	 * @throws UnsupportedOperationException if the property could not be binded
	 *             to the widget
	 */
	protected abstract void check();

	/**
	 * Check if the property can be binded to the widget, then build the widget.
	 *
	 * @param parent parent composite
	 * @return the created control
	 */
	public Control checkAndBuild(final Composite parent) {
		check();
		return build(parent);
	}

	/**
	 * Enable or disable the widget, depending on the associated enabler.
	 *
	 * @return true, if successful
	 */
	public boolean enableOrDisable() {
		if (this.enabler == null) {
			return true;
		}

		final boolean enabled = this.enabler.isEnabled();
		for (final Control c : this.controls) {
			if (!c.isDisposed()) {
				c.setEnabled(enabled);
			}
		}
		return enabled;
	}

	// ------------------------------- getters & setters

	/**
	 * Gets the alignment.
	 *
	 * @return the alignment (GridData.BEGINNING, GridData.CENTER, GridData.END,
	 *         GridData.FILL)
	 */
	public int getAlignment() {
		return this.alignment;
	}

	/**
	 * Gets the controls.
	 *
	 * @return the list of controls contained in the widget
	 */
	public List<Control> getControls() {
		return this.controls;
	}

	/**
	 * Checks if is grab excess space.
	 *
	 * @return <code>true</code> if the widget should grab the excess space
	 */
	public boolean isGrabExcessSpace() {
		return this.grabExcessSpace;
	}

	/**
	 * Gets the height.
	 *
	 * @return the height of the widget
	 */
	public int getHeight() {
		return this.height;
	}

	/**
	 * Gets the indent.
	 *
	 * @return the indentation space of the widget
	 */
	public int getIndent() {
		return this.indent;
	}

	/**
	 * Gets the label.
	 *
	 * @return the label associated to the widget (may be <code>null</code>)
	 */
	public String getLabel() {
		return this.label;
	}

	/**
	 * Gets the number of columns.
	 *
	 * @return the number of columns associated to the widget
	 */
	public int getNumberOfColumns() {
		return this.numberOfColumns;
	}

	/**
	 * Gets the property key.
	 *
	 * @return the propertyKey associated to the widget
	 */
	String getPropertyKey() {
		return this.propertyKey;
	}

	/**
	 * Gets the width.
	 *
	 * @return the width of the widget
	 */
	public int getWidth() {
		return this.width;
	}

	/**
	 * Checks if is single widget.
	 *
	 * @return <code>true</code> if the widget is "alone"
	 */
	public boolean isSingleWidget() {
		return this.singleWidget;
	}

	/**
	 * Adds a control to the list of control contained in the widget.
	 *
	 * @param control control to add
	 */
	protected void addControl(final Control control) {
		this.controls.add(control);
	}

	/**
	 * Sets the alignment.
	 *
	 * @param alignment the alignment to set (GridData.BEGINNING,
	 *            GridData.CENTER, GridData.END, GridData.FILL)
	 * @return the widget
	 */
	public PWWidget setAlignment(final int alignment) {
		if (alignment != GridData.BEGINNING && alignment != GridData.CENTER && alignment != GridData.END && alignment != GridData.FILL) {
			throw new UnsupportedOperationException("Value should be one of the following :GridData.BEGINNING, GridData.CENTER, GridData.END, GridData.FILL");
		}
		this.alignment = alignment;
		return this;
	}

	/**
	 * Sets the enabler.
	 *
	 * @param enabler the enabler to set
	 * @return the widget
	 */
	public PWWidget setEnabler(final Enabler enabler) {
		this.enabler = enabler;
		this.enabler.injectWidget(this);
		return this;
	}

	/**
	 * Sets the grab excess space.
	 *
	 * @param grabExcessSpace true if you want the widget to grab the excess
	 *            space
	 * @return the widget
	 */
	public PWWidget setGrabExcessSpace(final boolean grabExcessSpace) {
		this.grabExcessSpace = grabExcessSpace;
		return this;
	}

	/**
	 * Sets the height.
	 *
	 * @param height the height to set
	 * @return the widget
	 */
	public PWWidget setHeight(final int height) {
		this.height = height;
		return this;

	}

	/**
	 * Sets the indent.
	 *
	 * @param indent the indentation space to set
	 * @return the widget
	 */
	public PWWidget setIndent(final int indent) {
		this.indent = indent;
		return this;
	}

	/**
	 * Sets the width.
	 *
	 * @param width the width to set
	 * @return the widget
	 */
	public PWWidget setWidth(final int width) {
		this.width = width;
		return this;
	}

}
