/*******************************************************************************
 * Copyright (c) 2011 Laurent CARON.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Laurent CARON (laurent.caron@gmail.com) - initial API and implementation
 *******************************************************************************/
package org.mihalis.opal.transitionComposite;

/**
 * This enumeration is a list of transitions.
 */
public enum TRANSITIONS {
	
	/** The left to right. */
	LEFT_TO_RIGHT, 
 /** The left to right and appear. */
 LEFT_TO_RIGHT_AND_APPEAR, 
 /** The right to left. */
 RIGHT_TO_LEFT, 
 /** The right to left and appear. */
 RIGHT_TO_LEFT_AND_APPEAR, 
 /** The up to down. */
 UP_TO_DOWN, 
 /** The up to down and appear. */
 UP_TO_DOWN_AND_APPEAR, 
 /** The down to up. */
 DOWN_TO_UP, 
 /** The down to up and appear. */
 DOWN_TO_UP_AND_APPEAR, 
 /** The none. */
 NONE
}
