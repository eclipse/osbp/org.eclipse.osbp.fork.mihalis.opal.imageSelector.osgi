/*******************************************************************************
 * Copyright (c) 2012 Laurent CARON
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Laurent CARON (laurent.caron at gmail dot com) - initial API and implementation
 *******************************************************************************/
package org.mihalis.opal.obutton;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;
import org.mihalis.opal.utils.SWTGraphicUtil;

/**
 * This is the orange theme button renderer.
 */
public class OrangeButtonRenderer extends AbstractButtonRenderer {

	/** The instance. */
	private static OrangeButtonRenderer instance;
	
	/** The Constant FIRST_BACKGROUND_COLOR. */
	private static final Color FIRST_BACKGROUND_COLOR = SWTGraphicUtil.getColorSafely(255, 195, 117);
	
	/** The Constant SECOND_BACKGROUND_COLOR. */
	private static final Color SECOND_BACKGROUND_COLOR = SWTGraphicUtil.getColorSafely(238, 180, 79);

	/**
	 * Instantiates a new orange button renderer.
	 */
	private OrangeButtonRenderer() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.mihalis.opal.obutton.AbstractButtonRenderer#getFontColor()
	 */
	@Override
	protected Color getFontColor() {
		return Display.getDefault().getSystemColor(SWT.COLOR_WHITE);
	}

	/* (non-Javadoc)
	 * @see org.mihalis.opal.obutton.AbstractButtonRenderer#getFirstBackgroundColor()
	 */
	@Override
	protected Color getFirstBackgroundColor() {
		return FIRST_BACKGROUND_COLOR;
	}

	/* (non-Javadoc)
	 * @see org.mihalis.opal.obutton.AbstractButtonRenderer#getSecondBackgroundColor()
	 */
	@Override
	protected Color getSecondBackgroundColor() {
		return SECOND_BACKGROUND_COLOR;
	}

	/**
	 * Gets the single instance of OrangeButtonRenderer.
	 *
	 * @return single instance of OrangeButtonRenderer
	 */
	public static OrangeButtonRenderer getInstance() {
		if (instance == null) {
			instance = new OrangeButtonRenderer();
		}
		return instance;
	}
}
