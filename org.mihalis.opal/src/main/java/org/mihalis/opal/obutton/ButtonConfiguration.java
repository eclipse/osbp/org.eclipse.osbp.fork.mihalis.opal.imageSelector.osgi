/*******************************************************************************
 * Copyright (c) 2012 Laurent CARON
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Laurent CARON (laurent.caron at gmail dot com) - initial API and implementation
 *******************************************************************************/
package org.mihalis.opal.obutton;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;

/**
 * This class is a POJO used to configure a OButton widget.
 */
public class ButtonConfiguration {
	
	/** The font. */
	private Font font;
	
	/** The font color. */
	private Color fontColor;

	/** The corner radius. */
	private int cornerRadius;

	/** The gradient direction. */
	private int gradientDirection;
	
	/** The background color. */
	private Color backgroundColor;
	
	/** The second background color. */
	private Color secondBackgroundColor;

	/**
	 * Gets the font.
	 *
	 * @return the font
	 */
	public Font getFont() {
		return this.font;
	}

	/**
	 * Gets the font color.
	 *
	 * @return the fontColor
	 */
	public Color getFontColor() {
		return this.fontColor;
	}

	/**
	 * Gets the corner radius.
	 *
	 * @return the cornerRadius
	 */
	public int getCornerRadius() {
		return this.cornerRadius;
	}

	/**
	 * Gets the gradient direction.
	 *
	 * @return the gradientDirection
	 */
	public int getGradientDirection() {
		return this.gradientDirection;
	}

	/**
	 * Gets the background color.
	 *
	 * @return the backgroundColor
	 */
	public Color getBackgroundColor() {
		return this.backgroundColor;
	}

	/**
	 * Gets the second background color.
	 *
	 * @return the secondBackgroundColor
	 */
	public Color getSecondBackgroundColor() {
		return this.secondBackgroundColor;
	}

	/**
	 * Sets the font.
	 *
	 * @param font the font to set
	 * @return the button configuration
	 */
	public ButtonConfiguration setFont(final Font font) {
		if (font != null && font.isDisposed()) {
			SWT.error(SWT.ERROR_INVALID_ARGUMENT);
		}
		this.font = font;
		return this;
	}

	/**
	 * Sets the font color.
	 *
	 * @param fontColor the fontColor to set
	 * @return the button configuration
	 */
	public ButtonConfiguration setFontColor(final Color fontColor) {
		if (fontColor != null && fontColor.isDisposed()) {
			SWT.error(SWT.ERROR_INVALID_ARGUMENT);
		}

		this.fontColor = fontColor;
		return this;
	}

	/**
	 * Sets the corner radius.
	 *
	 * @param cornerRadius the cornerRadius to set
	 * @return the button configuration
	 */
	public ButtonConfiguration setCornerRadius(final int cornerRadius) {
		this.cornerRadius = Math.max(0, cornerRadius);
		return this;
	}

	/**
	 * Sets the gradient direction.
	 *
	 * @param gradientDirection the gradientDirection to set
	 * @return the button configuration
	 */
	public ButtonConfiguration setGradientDirection(final int gradientDirection) {
		if (gradientDirection != SWT.VERTICAL && gradientDirection != SWT.HORIZONTAL) {
			SWT.error(SWT.ERROR_INVALID_ARGUMENT);
		}
		this.gradientDirection = gradientDirection;
		return this;
	}

	/**
	 * Sets the background color.
	 *
	 * @param backgroundColor the backgroundColor to set
	 * @return the button configuration
	 */
	public ButtonConfiguration setBackgroundColor(final Color backgroundColor) {
		if (backgroundColor != null && backgroundColor.isDisposed()) {
			SWT.error(SWT.ERROR_INVALID_ARGUMENT);
		}
		this.backgroundColor = backgroundColor;
		return this;
	}

	/**
	 * Sets the second background color.
	 *
	 * @param secondBackgroundColor the secondBackgroundColor to set
	 * @return the button configuration
	 */
	public ButtonConfiguration setSecondBackgroundColor(final Color secondBackgroundColor) {
		if (secondBackgroundColor != null && secondBackgroundColor.isDisposed()) {
			SWT.error(SWT.ERROR_INVALID_ARGUMENT);
		}
		this.secondBackgroundColor = secondBackgroundColor;
		return this;
	}
}
