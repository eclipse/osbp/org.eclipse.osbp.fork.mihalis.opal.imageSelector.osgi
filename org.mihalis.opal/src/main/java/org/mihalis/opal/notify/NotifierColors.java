/*******************************************************************************
 * Copyright (c) 2011 Laurent CARON
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Laurent CARON (laurent.caron at gmail dot com) - initial API and implementation 
 *******************************************************************************/
package org.mihalis.opal.notify;

import org.eclipse.swt.graphics.Color;
import org.mihalis.opal.utils.SWTGraphicUtil;

/**
 * This class is a simple POJO that holds colors used by the Notifier widget.
 */
class NotifierColors {
	
	/** The title color. */
	Color titleColor;
	
	/** The text color. */
	Color textColor;
	
	/** The border color. */
	Color borderColor;
	
	/** The left color. */
	Color leftColor;
	
	/** The right color. */
	Color rightColor;

	/**
	 * Dispose.
	 */
	void dispose() {
		SWTGraphicUtil.safeDispose(this.titleColor);
		SWTGraphicUtil.safeDispose(this.borderColor);
		SWTGraphicUtil.safeDispose(this.leftColor);
		SWTGraphicUtil.safeDispose(this.rightColor);
		SWTGraphicUtil.safeDispose(this.textColor);
	}
}
