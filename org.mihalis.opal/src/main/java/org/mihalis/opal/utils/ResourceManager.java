package org.mihalis.opal.utils;

import java.util.ResourceBundle;

/**
 * The Class ResourceManager.
 */
public class ResourceManager {
	
	/** The Constant RSC. */
	private static final ResourceBundle RSC = ResourceBundle.getBundle("resources/opal");

	/** The Constant OK. */
	public static final String OK = "Ok";
	
	/** The Constant CANCEL. */
	public static final String CANCEL = "Cancel";
	
	/** The Constant CLOSE. */
	public static final String CLOSE = "Close";
	
	/** The Constant YES. */
	public static final String YES = "Yes";
	
	/** The Constant NO. */
	public static final String NO = "No";

	/** The Constant MEGABYTES. */
	public static final String MEGABYTES = "megabytes";
	
	/** The Constant PERFORM_GC. */
	public static final String PERFORM_GC = "performGC";

	/** The Constant LOGIN. */
	public static final String LOGIN = "login";
	
	/** The Constant NAME. */
	public static final String NAME = "name";
	
	/** The Constant PASSWORD. */
	public static final String PASSWORD = "password";					// NOSONAR
	
	/** The Constant REMEMBER_PASSWORD. */
	public static final String REMEMBER_PASSWORD = "rememberPassword";	// NOSONAR
	
	/** The Constant LOGIN_FAILED. */
	public static final String LOGIN_FAILED = "loginFailed";

	/** The Constant INPUT. */
	public static final String INPUT = "Input";
	
	/** The Constant APPLICATION_ERROR. */
	public static final String APPLICATION_ERROR = "ApplicationError";
	
	/** The Constant INFORMATION. */
	public static final String INFORMATION = "Information";
	
	/** The Constant WARNING. */
	public static final String WARNING = "Warning";
	
	/** The Constant CHOICE. */
	public static final String CHOICE = "Choice";
	
	/** The Constant EXCEPTION. */
	public static final String EXCEPTION = "Exception";
	
	/** The Constant SELECT. */
	public static final String SELECT = "Select";
	
	/** The Constant FEWER_DETAILS. */
	public static final String FEWER_DETAILS = "FewerDetails";
	
	/** The Constant MORE_DETAILS. */
	public static final String MORE_DETAILS = "MoreDetails";

	/** The Constant TIP_OF_THE_DAY. */
	public static final String TIP_OF_THE_DAY = "tipOfTheDay";
	
	/** The Constant DID_YOU_KNOW. */
	public static final String DID_YOU_KNOW = "didYouKnow";
	
	/** The Constant SHOW_TIP_AT_STARTUP. */
	public static final String SHOW_TIP_AT_STARTUP = "showTipAtStartup";
	
	/** The Constant PREVIOUS_TIP. */
	public static final String PREVIOUS_TIP = "previousTip";
	
	/** The Constant NEXT_TIP. */
	public static final String NEXT_TIP = "nextTip";

	/** The Constant CHOOSE. */
	public static final String CHOOSE = "choose";
	
	/** The Constant PREFERENCES. */
	public static final String PREFERENCES = "preferences";
	
	/** The Constant VALID_URL. */
	public static final String VALID_URL = "validURL";
	
	/** The Constant CHOOSE_DIRECTORY. */
	public static final String CHOOSE_DIRECTORY = "chooseDirectory";
	
	/** The Constant ITALIC. */
	public static final String ITALIC = "italic";
	
	/** The Constant BOLD. */
	public static final String BOLD = "bold";

	/** The Constant CATEGORY_SHORT_DESCRIPTION. */
	public static final String CATEGORY_SHORT_DESCRIPTION = "category.shortDescription";
	
	/** The Constant DESCRIPTION_SHORT_DESCRIPTION. */
	public static final String DESCRIPTION_SHORT_DESCRIPTION = "description.shortDescription";
	
	/** The Constant SORT_SHORT_DESCRIPTION. */
	public static final String SORT_SHORT_DESCRIPTION = "sort.shortDescription";
	
	/** The Constant PROPERTY. */
	public static final String PROPERTY = "property";
	
	/** The Constant VALUE. */
	public static final String VALUE = "value";
	
	/** The Constant EDIT_PROPERTY. */
	public static final String EDIT_PROPERTY = "editProperty";
	
	/** The Constant WIDTH. */
	public static final String WIDTH = "width";
	
	/** The Constant HEIGHT. */
	public static final String HEIGHT = "height";
	
	/** The Constant TOP. */
	public static final String TOP = "top";
	
	/** The Constant BOTTOM. */
	public static final String BOTTOM = "bottom";
	
	/** The Constant LEFT. */
	public static final String LEFT = "left";
	
	/** The Constant RIGHT. */
	public static final String RIGHT = "right";
	
	/** The Constant ERASE_PROPERTY. */
	public static final String ERASE_PROPERTY = "eraseProperty";

	/** The Constant PHYSICAL_MEMORY. */
	public static final String PHYSICAL_MEMORY = "physicalMemory";
	
	/** The Constant HEAP_MEMORY. */
	public static final String HEAP_MEMORY = "heapMemory";
	
	/** The Constant THREADS. */
	public static final String THREADS = "threads";
	
	/** The Constant CPU_USAGE. */
	public static final String CPU_USAGE = "cpuUsage";
	
	/** The Constant PEAK. */
	public static final String PEAK = "peak";
	
	/** The Constant MB. */
	public static final String MB = "mb";

	/** The Constant CALCULATOR_DIVIDE_BY_ZERO. */
	public static final String CALCULATOR_DIVIDE_BY_ZERO = "calculator.dividebyzero";
	
	/** The Constant CALCULATOR_INVALID_VALUE. */
	public static final String CALCULATOR_INVALID_VALUE = "calculator.invalid";

	/** The Constant MULTICHOICE_MESSAGE. */
	public static final String MULTICHOICE_MESSAGE = "multichoice.message";
	
	/** The Constant MULTICHOICE_MESSAGE_PLURAL. */
	public static final String MULTICHOICE_MESSAGE_PLURAL = "multichoice.message.plural";

	/**
	 * Get a translated label.
	 *
	 * @param key key to get
	 * @return the translated value of the key
	 */
	public static String getLabel(final String key) {
		return RSC.getString(key);
	}

}
