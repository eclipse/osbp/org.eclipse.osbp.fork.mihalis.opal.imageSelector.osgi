/*******************************************************************************
 * Copyright (c) 2012 Laurent CARON
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Laurent CARON (laurent.caron at gmail dot com) - initial API and implementation
 *******************************************************************************/
package org.mihalis.opal.systemMonitor;

import java.util.List;

import org.eclipse.swt.graphics.RGB;
import org.mihalis.opal.utils.FixedSizeQueue;

/**
 * Instances of this class are wrapper that contains a sample, its data, color,
 * caption, formatPattern...
 */
public class SampleWrapper {
	
	/** The color. */
	private RGB color;
	
	/** The border color. */
	private RGB borderColor;
	
	/** The caption. */
	private String caption;
	
	/** The format pattern. */
	private String formatPattern;
	
	/** The sample. */
	private final Sample sample;
	
	/** The data. */
	private final FixedSizeQueue<Double> data;
	
	/** The last value. */
	private Double lastValue;
	
	/** The last max value. */
	private Double lastMaxValue;
	
	/** The max value. */
	private Double maxValue;

	/**
	 * Constructor.
	 *
	 * @param sample associated sample
	 */
	SampleWrapper(final Sample sample) {
		super();
		this.sample = sample;
		this.color = new RGB(255, 255, 216);
		this.caption = "";
		this.formatPattern = "";
		this.data = new FixedSizeQueue<Double>(1000);
		this.lastValue = 0d;
		this.lastMaxValue = 0d;
		this.maxValue = 0d;
		createBorderColor();
	}

	/**
	 * Create the border color.
	 */
	private void createBorderColor() {
		this.borderColor = new RGB(Math.min(this.color.red * 2, 255), Math.min(this.color.green * 2, 255), Math.min(this.color.blue * 2, 255));
	}

	/**
	 * Collect a sample.
	 */
	void collect() {
		this.lastValue = this.sample.getValue();
		this.maxValue = Math.max(this.lastMaxValue, this.sample.getMaxValue());
		this.lastMaxValue = this.sample.getMaxValue();
		this.data.put(this.lastValue);
	}

	/**
	 * Gets the border color.
	 *
	 * @return the border color
	 */
	public RGB getBorderColor() {
		return this.borderColor;
	}

	/**
	 * Gets the caption.
	 *
	 * @return the caption
	 */
	public String getCaption() {
		return this.caption;
	}

	/**
	 * Gets the color.
	 *
	 * @return the color
	 */
	RGB getColor() {
		return this.color;
	}

	/**
	 * Gets the data.
	 *
	 * @return all data
	 */
	public List<Double> getData() {
		return this.data.getValues();
	}

	/**
	 * Gets the format pattern.
	 *
	 * @return the format pattern
	 */
	String getFormatPattern() {
		return this.formatPattern;
	}

	/**
	 * Gets the last max value.
	 *
	 * @return the last max value collected
	 */
	public Double getLastMaxValue() {
		return this.lastMaxValue;
	}

	/**
	 * Gets the last value.
	 *
	 * @return the last collected value
	 */
	public Double getLastValue() {
		return this.lastValue;
	}

	/**
	 * Gets the max value.
	 *
	 * @return the max value
	 */
	public double getMaxValue() {
		return this.maxValue;
	}

	/**
	 * Gets the number of collected elements.
	 *
	 * @return the number of collected elements
	 */
	public int getNumberOfCollectedElements() {
		return this.data.getSize();
	}

	/**
	 * Gets the sample.
	 *
	 * @return the sample
	 */
	Sample getSample() {
		return this.sample;
	}

	/**
	 * Resize.
	 *
	 * @param newSize new size of the data collector array
	 */
	public void resize(final int newSize) {
		this.data.resizeTo(newSize);
	}

	/**
	 * Sets the caption.
	 *
	 * @param caption the caption to set
	 * @return the sample wrapper
	 */
	SampleWrapper setCaption(final String caption) {
		this.caption = caption;
		return this;
	}

	/**
	 * Sets the color.
	 *
	 * @param color the color to set
	 * @return the sample wrapper
	 */
	SampleWrapper setColor(final RGB color) {
		this.color = color;
		createBorderColor();
		return this;
	}

	/**
	 * Sets the format pattern.
	 *
	 * @param formatPattern the format pattern to set
	 * @return the sample wrapper
	 */
	SampleWrapper setFormatPattern(final String formatPattern) {
		this.formatPattern = formatPattern;
		return this;
	}
}
