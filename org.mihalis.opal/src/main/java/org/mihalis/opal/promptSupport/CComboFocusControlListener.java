/*******************************************************************************
 * Copyright (c) 2011 Laurent CARON.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Laurent CARON (laurent.caron at gmail dot com) - Initial API and implementation
 *******************************************************************************/
package org.mihalis.opal.promptSupport;

import org.eclipse.swt.custom.CCombo;

/**
 * Focus/Control listener for a CCombo widget.
 *
 * @see CComboFocusControlEvent
 */
class CComboFocusControlListener extends BaseFocusControlListener {

	/**
	 * Constructor.
	 *
	 * @param control control on which this listener will be attached
	 */
	public CComboFocusControlListener(final CCombo control) {
		super(control);
	}

	/**
	 * Hide prompt.
	 *
	 * @see org.mihalis.opal.promptSupport.BaseFocusControlListener#hidePrompt()
	 */
	@Override
	protected void hidePrompt() {
		((CCombo) this.control).setText(EMPTY_STRING);
	}

	/**
	 * High light prompt.
	 *
	 * @see org.mihalis.opal.promptSupport.BaseFocusControlListener#highLightPrompt()
	 */
	@Override
	protected void highLightPrompt() {
	}

	/**
	 * Fill prompt text.
	 *
	 * @see org.mihalis.opal.promptSupport.BaseFocusControlListener#fillPromptText()
	 */
	@Override
	protected void fillPromptText() {
		final String promptText = PromptSupport.getPrompt(this.control);
		if (promptText != null) {
			((CCombo) this.control).setText(promptText);
		}
	}

	/**
	 * Checks if is filled.
	 *
	 * @return true, if is filled
	 * @see org.mihalis.opal.promptSupport.BaseFocusControlListener#isFilled()
	 */
	@Override
	protected boolean isFilled() {
		final String promptText = PromptSupport.getPrompt(this.control);
		if (promptText != null && promptText.equals(((CCombo) this.control).getText().trim())) {
			return false;
		}
		return !EMPTY_STRING.equals(((CCombo) this.control).getText().trim());
	}

}
