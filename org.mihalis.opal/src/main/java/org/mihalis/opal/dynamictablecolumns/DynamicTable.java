/*******************************************************************************
 * Copyright (c) 2011 Luis Carlos Moreira da Costa.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Luis Carlos Moreira da Costa (tcljava at gmail dot com) - initial API and implementation
 *******************************************************************************/
package org.mihalis.opal.dynamictablecolumns;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

/**
 * DynamicTable.
 */
public class DynamicTable extends Table {

	/** The parent. */
	private Composite parent = null;
	
	/** The container. */
	private Composite container = null;

	/** The header menu. */
	private Menu headerMenu = null;
	
	/** The dynamic layout. */
	private DynamicTableColumnLayout dynamicLayout = null;

	/**
	 * Constructor.
	 *
	 * @param parent Composite
	 * @param style int
	 */
	public DynamicTable(final Composite parent, final int style) {
		super(new Composite(parent, SWT.NONE) {
			@Override
			public void reskin(final int flags) {
				super.reskin(flags);
			}
		}, style);

		this.parent = parent;
		this.container = super.getParent();

		this.dynamicLayout = new DynamicTableColumnLayout() {
			@Override
			public void setColumnData(final DynamicColumnData column) {
				super.setColumnData(column);
				createMenuItem(DynamicTable.this.headerMenu, column);
			}
		};

		this.container.setLayout(this.dynamicLayout);
		this.headerMenu = new Menu(this.container.getShell(), SWT.POP_UP);
		addListener(SWT.MenuDetect, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				setMenu(((isMouseOverHeader(event.x, event.y)) ? DynamicTable.this.headerMenu : null));
			}
		});

		addListener(SWT.Dispose, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				DynamicTable.this.headerMenu.dispose();
			}
		});
	}

	/**
	 * Verify is mouse over header.
	 *
	 * @param x int
	 * @param y int
	 * @return boolean
	 */
	protected boolean isMouseOverHeader(final int x, final int y) {
		final Point pt = Display.getDefault().map(null, DynamicTable.this, new Point(x, y));
		final Rectangle clientArea = getClientArea();
		return (clientArea.y <= pt.y) && (pt.y < (clientArea.y + getHeaderHeight()));
	}

	/**
	 * Create menu item.
	 *
	 * @param menu Menu
	 * @param dynamicColumnData DynamicColumnData
	 */
	private void createMenuItem(final Menu menu, final DynamicColumnData dynamicColumnData) {
		final TableColumn tableColumn = dynamicColumnData.getTableColumn();
		final MenuItem itemName = new MenuItem(menu, SWT.CHECK);
		itemName.setText(tableColumn.getText());
		itemName.setSelection(tableColumn.getResizable());
		itemName.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				final boolean checked = itemName.getSelection();
				dynamicColumnData.setVisible(checked);
				tableColumn.setResizable(checked);
				layout();
			}
		});
	}

	/* (non-Javadoc)
	 * @see org.eclipse.swt.widgets.Composite#getLayout()
	 */
	@Override
	public DynamicTableColumnLayout getLayout() {
		return (DynamicTableColumnLayout) this.container.getLayout();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.swt.widgets.Composite#setLayout(org.eclipse.swt.widgets.Layout)
	 */
	@Override
	public void setLayout(final Layout layout) {
		throw new IllegalStateException();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.swt.widgets.Control#setLayoutData(java.lang.Object)
	 */
	@Override
	public void setLayoutData(final Object layoutData) {
		this.container.setLayoutData(layoutData);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.swt.widgets.Composite#layout()
	 */
	@Override
	public void layout() {
		this.container.layout();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.swt.widgets.Control#getParent()
	 */
	@Override
	public Composite getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.swt.widgets.Table#checkSubclass()
	 */
	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components.	
	}

}