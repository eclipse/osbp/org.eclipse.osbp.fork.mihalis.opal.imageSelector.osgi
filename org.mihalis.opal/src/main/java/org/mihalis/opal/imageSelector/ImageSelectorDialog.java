/*******************************************************************************
 * Copyright (c) 2000, 2012 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.mihalis.opal.imageSelector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

/**
 * Instances of this class allow the user to navigate the file system and select
 * or enter a file name.
 * <dl>
 * <dt><b>Styles:</b></dt>
 * <dd>SAVE, OPEN, MULTI</dd>
 * <dt><b>Events:</b></dt>
 * <dd>(none)</dd>
 * </dl>
 * <p>
 * Note: Only one of the styles SAVE and OPEN may be specified.
 * </p>
 * <p>
 * IMPORTANT: This class is <em>not</em> intended to be subclassed.
 * </p>
 * 
 * @see <a href="http://www.eclipse.org/swt/snippets/#filedialog">FileDialog
 *      snippets</a>
 * @see <a href="http://www.eclipse.org/swt/examples.php">SWT Example:
 *      ControlExample, Dialog tab</a>
 * @see <a href="http://www.eclipse.org/swt/">Sample code and further
 *      information</a>
 * @noextend This class is not intended to be subclassed by clients.
 */
public class ImageSelectorDialog extends Dialog {
	
	/** The image width. */
	int imageWidth;
	
	/** The filter extensions. */
	String[] filterExtensions = new String[0];

	/**
	 * Constructs a new instance of this class given only its parent.
	 *
	 * @param parent
	 *            a shell which will be the parent of the new instance
	 * @param imageWidth
	 *            the image width
	 * @exception IllegalArgumentException
	 *                <ul>
	 *                <li>ERROR_NULL_ARGUMENT - if the parent is null</li>
	 *                </ul>
	 * @exception SWTException
	 *                <ul>
	 *                <li>ERROR_THREAD_INVALID_ACCESS - if not called from the
	 *                thread that created the parent</li>
	 *                <li>ERROR_INVALID_SUBCLASS - if this class is not an
	 *                allowed subclass</li>
	 *                </ul>
	 */
	public ImageSelectorDialog(Shell parent, int imageWidth) {
		this(parent, SWT.APPLICATION_MODAL, imageWidth);
	}

	/**
	 * Constructs a new instance of this class given its parent and a style
	 * value describing its behavior and appearance.
	 * <p>
	 * The style value is either one of the style constants defined in class
	 * <code>SWT</code> which is applicable to instances of this class, or must
	 * be built by <em>bitwise OR</em>'ing together (that is, using the
	 * <code>int</code> "|" operator) two or more of those <code>SWT</code>
	 * style constants. The class description lists the style constants that are
	 * applicable to the class. Style bits are also inherited from superclasses.
	 * </p>
	 *
	 * @param parent
	 *            a shell which will be the parent of the new instance
	 * @param style
	 *            the style of dialog to construct
	 * @param imageWidth
	 *            the image width
	 * @see SWT#SAVE
	 * @see SWT#OPEN
	 * @see SWT#MULTI
	 * @exception IllegalArgumentException
	 *                <ul>
	 *                <li>ERROR_NULL_ARGUMENT - if the parent is null</li>
	 *                </ul>
	 * @exception SWTException
	 *                <ul>
	 *                <li>ERROR_THREAD_INVALID_ACCESS - if not called from the
	 *                thread that created the parent</li>
	 *                <li>ERROR_INVALID_SUBCLASS - if this class is not an
	 *                allowed subclass</li>
	 *                </ul>
	 */
	public ImageSelectorDialog(Shell parent, int style, int imageWidth) {
		super(parent, style);
		this.imageWidth = imageWidth;
	}

	/**
	 * Returns the file extensions which the dialog will use to filter the files
	 * it shows.
	 *
	 * @return the file extensions filter
	 */
	public String[] getFilterExtensions() {
		return filterExtensions;
	}

	/**
	 * Makes the dialog visible and brings it to the front of the display.
	 *
	 * @return a string describing the absolute path of the first selected file,
	 *         or null if the dialog was cancelled or an error occurred
	 *
	 * @exception SWTException
	 *                <ul>
	 *                <li>ERROR_WIDGET_DISPOSED - if the dialog has been
	 *                disposed</li>
	 *                <li>ERROR_THREAD_INVALID_ACCESS - if not called from the
	 *                thread that created the dialog</li>
	 *                </ul>
	 */
	public String open() {
		return open(false);
	}
	
	/**
	 * Open.
	 *
	 * @param returnSimpleName
	 *            the return simple name
	 * @return the string
	 */
	public String open(boolean returnSimpleName) {
		/* Make the parent shell be temporary modal */
		Display display = getParent().getDisplay();

		/*
		 * Open the dialog.
		 */
		final ImageSelector imageSelector = new ImageSelector(getParent(),
				getStyle(), imageWidth);
		imageSelector.setItems(filterExtensions);

		// Open the shell
		getParent().setSize(6*ImageSelector.WIDTH_FACTOR*imageWidth, 3*ImageSelector.WIDTH_FACTOR*imageWidth);
		getParent().open();
		while (!getParent().isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		imageSelector.clearItems();

		if (returnSimpleName) {
			return imageSelector.getImageName();
		} else {
			return imageSelector.getImagePlatformPath();
		}
	}

	/**
	 * Set the file extensions which the dialog will use to filter the files it
	 * shows to the argument, which may be null.
	 * <p>
	 * The strings are platform specific. For example, on some platforms, an
	 * extension filter string is typically of the form "*.extension", where
	 * "*.*" matches all files. For filters with multiple extensions, use
	 * semicolon as a separator, e.g. "*.jpg;*.png".
	 * </p>
	 *
	 * @param extensions
	 *            the file extension filter
	 * 
	 * @see #setFilterNames to specify the user-friendly names corresponding to
	 *      the extensions
	 */
	public void setFilterExtensions(String[] extensions) {
		filterExtensions = extensions;
	}
}
