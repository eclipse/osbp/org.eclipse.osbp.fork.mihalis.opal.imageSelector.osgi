/*******************************************************************************
 * Copyright (c) 2011 Laurent CARON
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Laurent CARON (laurent.caron at gmail dot com) - Initial implementation and API
 *******************************************************************************/
package org.eclipse.osbp.fork.mihalis.opal.imageSelector;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.mihalis.opal.OpalItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Instances of this class represents items manipulated by the ImageSelector
 * widget.
 */
public class ISItem extends OpalItem implements Comparable<ISItem> {

	/** The z position. */
	private double zPosition;
	
	/** The upper left corner. */
	private Point upperLeftCorner;
	
	/** The lower right corner. */
	private Point lowerRightCorner;
	
	/** The url. */
	private URL url;
	
	/** The swt image. */
	private Image swtImage;
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger( ISItem.class );

	/**
	 * Constructor.
	 *
	 * @param imgFile
	 *            the img file
	 */
	public ISItem(final File imgFile) {
		try (InputStream is = imgFile.toURI().toURL().openStream()) {
			swtImage = new Image( Display.getCurrent(), is );
			setImage( swtImage );
			setText(FilenameUtils.removeExtension(imgFile.getName()));
			setUrl(imgFile.toURI().toURL());
		} catch (IOException e) {
			LOGGER.error(e.getLocalizedMessage());
		}

	}
	
	/**
	 * Dispose SWT image.
	 */
	public void dispose () {
		swtImage.dispose();
	}
	
	/**
	 * Gets the url.
	 *
	 * @return the url
	 */
	public URL getUrl() {
		return url;
	}

	/**
	 * Sets the url.
	 *
	 * @param url
	 *            the new url
	 */
	public void setUrl(URL url) {
		this.url = url;
	}

	/**
	 * Gets the z position.
	 *
	 * @return the zPosition
	 */
	double getzPosition() {
		return this.zPosition;
	}

	/**
	 * Setz position.
	 *
	 * @param zPosition
	 *            the zPosition to set
	 * @return the checks if is item
	 */
	ISItem setzPosition(final double zPosition) {
		this.zPosition = zPosition;
		return this;
	}

	/**
	 * Gets the upper left corner.
	 *
	 * @return the upperLeftCorner
	 */
	Point getUpperLeftCorner() {
		return this.upperLeftCorner;
	}

	/**
	 * Sets the upper left corner.
	 *
	 * @param x
	 *            the upperLeftCorner.x to set
	 * @param y
	 *            the upperLeftCorner.y to set
	 */
	void setUpperLeftCorner(final int x, final int y) {
		this.upperLeftCorner = new Point(x, y);
	}

	/**
	 * Gets the lower right corner.
	 *
	 * @return the lowerRightCorner
	 */
	Point getLowerRightCorner() {
		return this.lowerRightCorner;
	}

	/**
	 * Sets the lower right corner.
	 *
	 * @param x
	 *            the lowerRightCorner.x to set
	 * @param y
	 *            the lowerRightCorner.y to set
	 */
	void setLowerRightCorner(final int x, final int y) {
		this.lowerRightCorner = new Point(x, y);
	}

	/**
	 * Reset corner to null.
	 */
	void resetCornerToNull() {
		this.upperLeftCorner = null;
		this.lowerRightCorner = null;

	}

	/**
	 * To string.
	 *
	 * @return the string
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CCISItem [getText()=" + this.getText() + "]";
	}

	/**
	 * Compare to.
	 *
	 * @param o
	 *            the o
	 * @return the int
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(final ISItem o) {
//		return new Double(Math.abs(this.zPosition)).compareTo(Math.abs(o
//				.getzPosition())) * -1;
		return this.getText().compareTo(o.getText());
	}

	/**
	 * Indicates whether some other {@code CCISItem} is "equal" to this one. Two
	 * objects of type {@code CCISItem} are considered "equal" if their
	 * attributes {@literal zPosition} are equal.
	 * 
	 * If the passed object is null or <b>not</b> of type {@code CCISItem} the
	 * method returns {@code false}. Returns {@code true} if the result of
	 * method {@code compareTo()} is 0} .
	 * 
	 * This method is overridden so as to maintain the general contract for
	 * classes that implement the {@code Comparable} interface.
	 *
	 * @author gu
	 * @param o
	 *            the reference object with which to compare (should be of type
	 *            CCISItem).
	 * @return {@code true} if this {@code CCISItem} is the same as the passed
	 *         object of type {@code CCISItem}; {@code false} otherwise.
	 * @see java.lang.Object#equals(java.lang.Object)
	 * @see compareTo
	 */
	@Override
	public boolean equals ( Object o ) {
		if	( o == null )
			return	false;

		if	( o.getClass() != this.getClass() )
			return	false;

		return	compareTo( (ISItem) o ) == 0;
	}

	/**
	 * Returns a hash code value for the object, which is the result of passing
	 * attribute {@literal zPosition} to method {@code Math.ceil()}.
	 * 
	 * This method is overridden so as to maintain the general contract for
	 * classes that implement the {@code Comparable} interface.
	 *
	 * @author gu
	 * @return a hash code value for this object.
	 * @see java.lang.Math.ceil(double)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return	(int) Math.ceil( zPosition );
	}

}
